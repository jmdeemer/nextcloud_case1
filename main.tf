#######################
#---# AWS REGIONS #---#
#######################

#Spin up servers 
module "aws-us-east-1" { # Virginia
  source        = "./aws_module"
  region        = "us-east-1"
  servers_count = 1
}

#########################
#---# Azure REGIONS #---#
#########################
#
#module "azure-east-us-1" { # Virginia
#  source   = "./azure-module"
#  location = "East US"
#}
#
##########################
#---# Google REGIONS #---#
##########################