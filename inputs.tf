variable "server_amount" {
  description = "Number of servers to provision"
  type        = number
  default     = 1
}