######################
#---# AWS OUTPUT #---#
######################

## AWS Virginia

output "aws-us-east-1-public_ips_v4" {
  value = module.aws-us-east-1.public_ips_v4
}

output "aws-us-east-1-private_ips_v4" {
  value = module.aws-us-east-1.private_ips_v4
}

output "aws-us-east-1-keypair" {
  value     = module.aws-us-east-1.keypair
  sensitive = true
}

output "aws-us-east-1-uuid" {
  value = module.aws-us-east-1.uuid
}

output "aws-us-east-1-selected_az" {
  value = module.aws-us-east-1.selected_az
}

#output "turn-server-secret" {
#  value = "4e2f2f4723a8b948881e25f7d030c5346f62fffc126c93a78045e2c36d628714"
#}

#output "aws-us-east-1-dns_records" {
#  value = module.aws-us-east-1.dns_records
#}

