provider "aws" {
  profile = "neta-jus"
  region = "us-east-1"
}

data "aws_availability_zones" "available" {
  state         = "available"
  exclude_names = var.exclude_az #uncomment to exclude specific az
}

resource "random_shuffle" "az" {
  input        = "${tolist(data.aws_availability_zones.available.names)}"
  result_count = 1
}

#data "aws_route53_zone" "pfsense" {
#  name = var.r53_zone_name
#}

data "aws_ami" "pfsense" {
  most_recent = true

  filter {
    name   = "name"
    values = ["pfSense-plus-ec2-21.05.1-RELEASE-amd64-d6a66a49-ceec-4a27-ad5b-ea8a3eb55b15"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["679593333241"] # Canonical
}

resource "aws_vpc" "netenclave-pfsense" {
  count                = var.servers_count
  cidr_block           = var.vpc_cidrblock
  enable_dns_support   = "true"
  enable_dns_hostnames = "true"

  tags = {
    Name = "netenclave-pfsense-${var.region}"
  }
}

resource "aws_internet_gateway" "ob_connections" {
  count  = var.servers_count
  vpc_id = element(aws_vpc.netenclave-pfsense.*.id, count.index)

  tags = {
    Name = "netenclave-pfsense-${var.region}-igw"
  }
}

resource "aws_subnet" "public" {
  count                   = var.servers_count
  vpc_id                  = element(aws_vpc.netenclave-pfsense.*.id, count.index)
#  cidr_block              = cidrsubnet(element(aws_vpc.netenclave-pfsense.*.public_cidr, count.index), 8, count.index)
  cidr_block              = "172.18.0.0/24"
  map_public_ip_on_launch = true
  availability_zone       = "${random_shuffle.az.result[0]}"

  tags = {
    Name = "netenclave-pfsense-${random_shuffle.az.result[0]}-public"
  }
}

resource "aws_subnet" "private" {
  count                   = var.servers_count
  vpc_id                  = element(aws_vpc.netenclave-pfsense.*.id, count.index)
  cidr_block              = "172.18.1.0/24"
#  cidr_block              = cidrsubnet(element(aws_vpc.netenclave-pfsense.*.private_cidr, count.index), 8, count.index)
  map_public_ip_on_launch = true
  availability_zone       = "${random_shuffle.az.result[0]}"

  tags = {
    Name = "netenclave-pfsense-${random_shuffle.az.result[0]}-private"
  }
}

resource "aws_route" "default_table" {
  count                  = var.servers_count
  route_table_id         = element(aws_vpc.netenclave-pfsense.*.default_route_table_id, count.index)
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = element(aws_internet_gateway.ob_connections.*.id, count.index)
}

resource "aws_route_table_association" "public" {
  count          = var.servers_count
  subnet_id      = element(aws_subnet.public.*.id, count.index)
  #route_table_id = element(aws_route_table.public.*.id, count.index)
  route_table_id = element(aws_vpc.netenclave-pfsense.*.default_route_table_id, count.index)
}

resource "aws_route_table_association" "private" {
  count          = var.servers_count
  subnet_id      = element(aws_subnet.private.*.id, count.index)
  #route_table_id = element(aws_route_table.public.*.id, count.index)
  route_table_id = element(aws_vpc.netenclave-pfsense.*.default_route_table_id, count.index)
}

resource "aws_security_group" "pfsense" {
  description = "pfSense ACL"
  count       = var.servers_count
  vpc_id      = element(aws_vpc.netenclave-pfsense.*.id, count.index)

  ingress {
    description = "allow ping IPv4"
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "remote administration"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [
      "0.0.0.0/0",
      "216.98.74.229/32",
      "71.163.61.49/32",
      "72.202.212.141/32"
    ]
  }

  ingress {
    description = "web access"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "secure web access"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "allow intercommunication"
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    self      = true
  }

  egress {
    description = "outbound connectivity"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "netenclave-pfsense-${var.region}-securitygroup"
  }
}

resource "tls_private_key" "access_key_params" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "access_credentials" {
  key_name_prefix = "${var.region}-keys"
  public_key      = tls_private_key.access_key_params.public_key_openssh

  provisioner "local-exec" {
    command     = "echo '${tls_private_key.access_key_params.private_key_pem}' >> ${self.key_name}.pem"
    working_dir = "./aws_keys"
  }

  provisioner "local-exec" {
    command     = "chmod 600 ${self.key_name}.pem"
    working_dir = "./aws_keys"
  }

  provisioner "local-exec" {
    when        = destroy
    command     = "rm ${self.key_name}.pem"
    working_dir = "./aws_keys"
  }
}

resource "random_id" "pfsense" {
  count       = var.servers_count
  byte_length = 8
}

resource "aws_instance" "pfsense" {
  count                  = var.servers_count
  instance_type          = var.instance_type_nano
  ami                    = data.aws_ami.pfsense.id
  key_name               = aws_key_pair.access_credentials.key_name

  credit_specification {
    cpu_credits = "unlimited"
  }

  network_interface {
    network_interface_id = element(aws_network_interface.primary_interface.*.id, count.index)
    device_index         = 0
  }

  network_interface {
    network_interface_id = element(aws_network_interface.secondary_interface.*.id, count.index)
    device_index         = 1
  }

  lifecycle {
    create_before_destroy = true
  }

  tags = {
    Name = "netenclave-pfsense-${element(data.aws_availability_zones.available.names, count.index)}-${count.index}-${element(random_id.pfsense.*.hex, count.index)}"
  }

  #provisioner "remote-exec" {
  #  inline = [
  #    "sudo apt update",
  #    "sudo apt install python3 -y",
  #    "echo Done!"
  #  ]

  #  connection {
  #    host        = self.public_ip
  #    type        = "ssh"
  #    user        = "ubuntu"
  #    private_key = file("./aws_keys/${aws_key_pair.access_credentials.key_name}.pem")
  #  }
  #}

  #provisioner "local-exec" {
  #  command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -vvv -u ubuntu -i '${self.public_ip},' --private-key './aws_keys/${aws_key_pair.access_credentials.key_name}.pem' ./ansible/nextcloud.yaml -e 'dns_name=dev-net-enclave.duckdns.org'"
  #}

#  depends_on = [aws_eip.public_ipv4, aws_eip_association.public_ipv4, aws_network_interface.primary_interface, aws_route53_record.public_ipv4]
  depends_on = [aws_eip.public_ipv4, aws_eip_association.public_ipv4, aws_network_interface.primary_interface, aws_network_interface.secondary_interface]
}

resource "aws_eip" "public_ipv4" {
  vpc        = true
  count      = var.servers_count
  depends_on = [aws_internet_gateway.ob_connections]
}

resource "aws_network_interface" "primary_interface" {
  count           = var.servers_count
  subnet_id       = element(aws_subnet.public.*.id, count.index)
  security_groups = [element(aws_security_group.pfsense.*.id, count.index)]
}

resource "aws_network_interface" "secondary_interface" {
  count           = var.servers_count
  subnet_id       = element(aws_subnet.private.*.id, count.index)
  security_groups = [element(aws_security_group.pfsense.*.id, count.index)]
}

resource "aws_eip_association" "public_ipv4" {
  count                = var.servers_count
  allocation_id        = element(aws_eip.public_ipv4.*.id, count.index)
  network_interface_id = element(aws_network_interface.primary_interface.*.id, count.index)
}

#resource "aws_route53_record" "public_ipv4" {
#  count          = var.servers_count
#  zone_id        = data.aws_route53_zone.pfsense.zone_id
#  name           = format(
#                     "%s.%s",
#                     "${var.r53_domain_name}-${element(random_id.pfsense.*.hex, count.index)}",
#                     data.aws_route53_zone.pfsense.name,
#                   )
#  type           = "A"
#  ttl            = "300"
#  records        = ["${element(aws_eip.public_ipv4.*.public_ip, count.index)}"]
#}
