variable "region" {
  default = "us-east-1"
}

variable "profile" {
  default = "default"
}

variable "servers_count" {
  default = 1
}

variable "vpc_cidrblock" {
  default = "172.18.0.0/16"
}

variable "public_cidrblock" {
  default = "172.18.0.0/24"
}

variable "private_cidrblock" {
  default = "172.18.1.0/24"
}


variable "instance_type_small" {
  default = "t3.small"
}

variable "instance_type_nano" {
  default = "t2.nano"
}

variable "r53_zone_name" {
  default = "dev.netenclave.io"
}

variable "r53_domain_name" {
  default = "filexchange"
}

variable "exclude_az" {
  default = ["us-east-1e"]
}
