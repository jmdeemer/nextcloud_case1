output "keypair" {
  value     = tls_private_key.access_key_params.private_key_pem
  sensitive = true
}

output "public_ips_v4" {
  value = aws_instance.pfsense.*.public_ip
}

output "private_ips_v4" {
  value = aws_instance.pfsense.*.private_ip
}

output "uuid" {
  value = random_id.pfsense.*.hex
}

output "selected_az" {
  value = random_shuffle.az.*.result
}

#output "turn-server-secret" {
#  value = "4e2f2f4723a8b948881e25f7d030c5346f62fffc126c93a78045e2c36d628714"
#}

#output "dns_records" {
#  value = aws_route53_record.public_ipv4.*.name
#}